class Api::UsersController < ApplicationController
    before_action :findUser, only: [:showUser, :updateUser, :deleteUser]
    def getUsers
        users = User.all
        if users
            render json: users, status: :ok
        else
            render json: {msg: "no users found"}, status: :unprocessable_entity
        end
    end
    def addUsers
        user = User.new(userparams)
        if user.save
            render json: user, status: :ok
        else
            render json: {msg:"user not added"}, status: :unprocessable_entity
        end
    end

    def showUser
    
        if @user
            render json: @user, status: :ok
        else
            render json: {msg:"user not found"}, status: :unprocessable_entity
        end
    end

    def updateUser

        if @user
            if @user.update(userparams)
                render json: @user, status: :ok
            else
                render json: {msg:"failed to update"}, status: :unprocessable_entity
            end
        else
            render json: {msg:"no users found"}, status: :unprocessable_entity
        end
    end

    def deleteUser
        
        if @user
            if @user.destroy
                render json: {msg: "user deleted"}, status: :ok
            else
                render json: {msg:"failed to delete"}, status: :unprocessable_entity
            end
        else
            render json: {msg:"no users found"}, status: :unprocessable_entity
        end

    end

    def search
        if params[:input].blank?
            render json: {msg:"no results found"}, status: :unprocessable_entity
        else
            @parameter = params[:input]
            @result = User.or({firstName: /#{@parameter}/i}, {lastName: /#{@parameter}/i}, {email: /#{@parameter}/i})
            render json: @result, status: :ok
        end 
    end

    private
    def userparams
        params.permit(:firstName, :lastName, :email)
    end 

    def findUser
        @user = User.find(params[:id])
    end

end