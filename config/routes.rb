Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api do
    post 'user', action: :addUsers, controller: :users
    get 'users', action: :getUsers, controller: :users
    get 'user/:id', action: :showUser, controller: :users
    put 'user/:id', action: :updateUser, controller: :users
    delete 'user/:id', action: :deleteUser, controller: :users
    get 'typeahead/:input', action: :search, controller: :users
  end
end
